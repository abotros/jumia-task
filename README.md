# Jumia Task

A Lumen backend application for filtering and validating phone numbers according to provided countries patterns, and
front end app using angular and bootstrap to get the results from the backend.

Lumen means the smaller, simpler, leaner and faster version of the complete web application framework. Lumen framework
has a foundation level that is at the same level as Laravel, with most of the similar components. Lumen has less
configuration and different routing parameters in configuring the web applications and helps in the fast development
with speed.

## Description

The application reads the customers records from an SQLite database and process them, and then exposes a REST API for
accessing the data.

The flow goes:

1. Open `localhost:4200` to open the frontend page.
2. The frontend calls `/phone-numbers` endpoint in the backend to get the numbers after being validated.
3. The user can can filter the phone numbers by country code by calling `/phone-numbers/{country-code}` or by valid or
   not valid by calling the same endpoints adding query params `?state=valid` or `?state=notValid`.

## Technical Decisions

1. Lumen is used for backend for simplicity and speed.
2. Angular is used for the frontend for its simplicity and huge community and support.
3. Filtering was implemented in the backend to be scalable if the number of customer grows, also I implemented filtering
   by front-end in a different branch as the given number of the customers is very small so no need to send http
   requests on each change.

## How To Run
1. git clone --recursive https://bitbucket.org/abotros/jumia-task.git 
2. cd jumia-task
3. Run `docker-compose up --build`.
4. Open `localhost:4200` from the browser.

## Future Work

Add pagination so the response is faster by less content and also not to exhaust the calls to the database and the front
end memory.
